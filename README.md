# README #

This is an adapter package for using Hspec tests with MuCheck. To use it,
checkout mucheck in another directory and do from the root of
mucheck-hspec

```
$ cabal sandbox init
$ cabal sandbox add-source ../mucheck
$ cabal install --dependencies-only
$ cabal configure
$ cabal build
```

You can also reuse the premade cabal sandbox from mucheck if you have already
installed it

```
$ cabal sandbox init --sandbox ../mucheck
$ cabal configure
$ cabal build
```
Using it.

```
$ cabal repl
> :l src/Main.hs
> :m + Test.MuCheck.TestAdapter
> :m + Test.MuCheck.TestAdapter.Hspec
> mucheck (toRun "Examples/HspecTest.hs" :: HspecRun) [] >>= return . fst

If you have the HPC tix file, then

> mucheck (toRun "Examples/HspecTest.hs" :: HspecRun) "sample-test.tix" >>= return . fst

```
